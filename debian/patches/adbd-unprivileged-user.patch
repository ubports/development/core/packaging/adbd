From: Guido Berhoerster <guido+ubports@berhoerster.name>
Date: Thu, 6 Jun 2024 02:38:25 +0700
Subject: Run adbd as the phablet user

This transitions the UID/GID to the phablet user and uses the setuid
adbd-pam-session help in order to run a shell in a new PAM session.

[ratchanan@ubports.com: forward-port to 14.0.0+r45]
---
 adb.h                    |  6 ++++++
 daemon/main.cpp          | 43 +++++++++++++++++++++++++++++++++++++++----
 daemon/shell_service.cpp | 10 +++++++---
 3 files changed, 52 insertions(+), 7 deletions(-)

diff --git a/adb.h b/adb.h
index 0911f46..f140d9a 100644
--- a/adb.h
+++ b/adb.h
@@ -29,6 +29,12 @@
 #include "socket.h"
 #include "types.h"
 
+#if !ADB_HOST
+#ifndef UBUNTU_PHABLET
+#define UBUNTU_PHABLET 32011 // Ubuntu touch phablet user
+#endif
+#endif
+
 constexpr size_t MAX_PAYLOAD_V1 = 4 * 1024;
 constexpr size_t MAX_PAYLOAD = 1024 * 1024;
 constexpr size_t MAX_FRAMEWORK_PAYLOAD = 64 * 1024;
diff --git a/daemon/main.cpp b/daemon/main.cpp
index fc25fd9..0d94c25 100644
--- a/daemon/main.cpp
+++ b/daemon/main.cpp
@@ -29,6 +29,9 @@
 #include <stdio.h>
 #include <stdlib.h>
 #include <sys/capability.h>
+#include <pwd.h>
+#include <grp.h>
+#include <unistd.h>
 #include <sys/prctl.h>
 
 #include <memory>
@@ -97,10 +100,44 @@ static bool should_drop_privileges() {
     return drop;
 }
 
+#endif
+
 static void drop_privileges(int server_port) {
-    // Change user to phablet in the future
+    if (geteuid() == 0) {
+        uid_t ruid, euid, suid;
+        gid_t rgid, egid, sgid;
+
+        // initialize group access list with supplementary groups
+        // UBUNTU_PHABLET user is a member of
+        struct passwd *pw = getpwuid(UBUNTU_PHABLET);
+        if (pw) {
+            if (initgroups(pw->pw_name, pw->pw_gid) != 0) {
+                PLOG(FATAL) << "initgroups";
+            }
+        }
+
+        // transition UID/GID to UBUNTU_PHABLET user
+        if (setresgid(UBUNTU_PHABLET, UBUNTU_PHABLET, UBUNTU_PHABLET) != 0) {
+            PLOG(FATAL) << "setresgid";
+        }
+        if (getresgid(&rgid, &egid, &sgid) != 0) {
+            PLOG(FATAL) << "getresgid";
+        }
+        if ((rgid != UBUNTU_PHABLET) || (egid != UBUNTU_PHABLET) || (sgid != UBUNTU_PHABLET)) {
+            LOG(FATAL) << "failed to change gid";
+        }
+
+        if (setresuid(UBUNTU_PHABLET, UBUNTU_PHABLET, UBUNTU_PHABLET) != 0) {
+            PLOG(FATAL) << "setresuid";
+        }
+        if (getresuid(&ruid, &euid, &suid) != 0) {
+            PLOG(FATAL) << "getresuid";
+        }
+        if ((ruid != UBUNTU_PHABLET) || (euid != UBUNTU_PHABLET) || (suid != UBUNTU_PHABLET)) {
+            LOG(FATAL) << "failed to change uid";
+        }
+    }
 }
-#endif
 
 static void setup_adb(const std::vector<std::string>& addrs) {
 #if defined(__ANDROID__)
@@ -169,9 +206,7 @@ int adbd_main(int server_port) {
           " unchanged.\n");
     }
 
-#if defined(__ANDROID__)
     drop_privileges(server_port);
-#endif
 
 #if defined(__ANDROID__)
     // A thread gets spawned as a side-effect of initializing the watchdog, so it needs to happen
diff --git a/daemon/shell_service.cpp b/daemon/shell_service.cpp
index c566bab..f72594a 100644
--- a/daemon/shell_service.cpp
+++ b/daemon/shell_service.cpp
@@ -110,6 +110,10 @@
 #include "security_log_tags.h"
 #include "shell_protocol.h"
 
+#ifndef PATH_PAM_HELPER
+#define PATH_PAM_HELPER "/usr/libexec/adbd-pam-session"
+#endif
+
 namespace {
 
 // Reads from |fd| until close or failure.
@@ -381,11 +385,11 @@ bool Subprocess::ForkAndExec(std::string* error) {
 
         if (command_.empty()) {
             // Spawn a login shell if we don't have a command.
-            execle(_PATH_BSHELL, "-" _PATH_BSHELL, nullptr, cenv.data());
+            execle(PATH_PAM_HELPER, PATH_PAM_HELPER, nullptr, cenv.data());
         } else {
-            execle(_PATH_BSHELL, _PATH_BSHELL, "-c", command_.c_str(), nullptr, cenv.data());
+            execle(PATH_PAM_HELPER, PATH_PAM_HELPER, command_.c_str(), nullptr, cenv.data());
         }
-        WriteFdExactly(child_error_sfd, "exec '" _PATH_BSHELL "' failed: ");
+        WriteFdExactly(child_error_sfd, "exec '" PATH_PAM_HELPER "' failed: ");
         WriteFdExactly(child_error_sfd, strerror(errno));
         child_error_sfd.reset(-1);
         _Exit(1);
