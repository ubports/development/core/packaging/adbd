#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk
include /usr/share/dpkg/pkg-info.mk

export DEB_HOST_MULTIARCH
export DEB_CFLAGS_MAINT_APPEND = -fPIC
export DEB_CXXFLAGS_MAINT_APPEND = -fPIC -std=c++2a
export DEB_CPPFLAGS_MAINT_APPEND = -DNDEBUG -UDEBUG -I/usr/include/android
export DEB_LDFLAGS_MAINT_APPEND = -fPIC

# Workaround https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1016936
# Clang starts to use DWARFv5 which dwz doesn't support yet, which results in
# dwz failing with "Unknown debugging section .debug_addr" and subsequently
# "Too few files for multifile optimization".
export DEB_CFLAGS_MAINT_APPEND += -gdwarf-4
export DEB_CXXFLAGS_MAINT_APPEND += -gdwarf-4

# Workaround https://pad.lv/2064187, which breaks LTO on LLVM 18.
# The fix is supposedly in progress, so only disable LTO until the current
# version is superseded.
llvm_18_version := $(shell dpkg-query --showformat='$${Version}' --show llvm-18-linker-tools)
ifneq ($(llvm_18_version),)
ifeq ($(shell dpkg --compare-versions "$(llvm_18_version)" le "1:18.1.3-1" && echo 1), 1)
export DEB_BUILD_MAINT_OPTIONS = optimize=-lto
endif
endif

export DEB_VERSION
# DEB_BUILD_PROFILES = stage1

# When building adbd, it's easier to just use Clang instead of trying to figure
# out how to patch certain parts to build under GCC.
export CC = clang -target $(DEB_HOST_GNU_TYPE)
export CXX = clang++ -target $(DEB_HOST_GNU_TYPE)

COMPONENTS = \
		adbd

.PHONY: adbd

adbd: debian/adbd.mk
	dh_auto_build --buildsystem=makefile -- -f $<

%:
	dh $@

override_dh_auto_configure-arch:
	mkdir -p debian/out
ifneq ($(filter amd64 i386 armel armhf arm64 mipsel mips64el ppc64el riscv64,$(DEB_HOST_ARCH)),)
	for proto in ./proto; do \
		(cd $$proto && \
			find . -name '*.proto' -printf 'Regenerate %p\n' \
			-exec protoc --cpp_out=. {} \;) \
	done
endif


override_dh_auto_build-arch: $(COMPONENTS)
	dh_auto_build -D debian/adbd-pam-session

override_dh_auto_install:
	dh_auto_install --destdir=debian/tmp -D debian/adbd-pam-session

override_dh_fixperms:
	dh_fixperms --exclude=usr/libexec/adbd-pam-session

override_dh_auto_clean:
	dh_auto_clean
	dh_auto_clean -D debian/adbd-pam-session
	$(RM) debian/*.1

override_dh_shlibdeps:
	dh_shlibdeps -l/usr/lib/$(DEB_HOST_MULTIARCH)/android

override_dh_installsystemd:
	dh_installsystemd --no-restart-on-upgrade --no-restart-after-upgrade
